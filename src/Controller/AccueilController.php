<?php

// src/Controller/AccueilController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends Controller
{
    /**
     * @Route("/accueil", name="accueil")
     */
    public function accueilInfos()
    {

        $userInformations = "Bonjour Vincent";

        return $this->render('accueil/accueil.html.twig', array(
            'userInformations' => $userInformations
        ));
    }

}

?>
