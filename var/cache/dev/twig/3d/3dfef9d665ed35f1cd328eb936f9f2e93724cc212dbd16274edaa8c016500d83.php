<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_ee1f135136763396ec92818e24c00b59bfd38f5bce7c283c2c7dec0a05290dbb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_773815e855c0e1db04b17ecde6a89827f06d21e2bea579b60acb3a07060c5245 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_773815e855c0e1db04b17ecde6a89827f06d21e2bea579b60acb3a07060c5245->enter($__internal_773815e855c0e1db04b17ecde6a89827f06d21e2bea579b60acb3a07060c5245_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_041df3a584d1ed832429ed7c3032491349a438d455d9780f68bd4510f6d3aa64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_041df3a584d1ed832429ed7c3032491349a438d455d9780f68bd4510f6d3aa64->enter($__internal_041df3a584d1ed832429ed7c3032491349a438d455d9780f68bd4510f6d3aa64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_773815e855c0e1db04b17ecde6a89827f06d21e2bea579b60acb3a07060c5245->leave($__internal_773815e855c0e1db04b17ecde6a89827f06d21e2bea579b60acb3a07060c5245_prof);

        
        $__internal_041df3a584d1ed832429ed7c3032491349a438d455d9780f68bd4510f6d3aa64->leave($__internal_041df3a584d1ed832429ed7c3032491349a438d455d9780f68bd4510f6d3aa64_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_064c50efc4124fd4f64b42c73e16645a330d7dc6a7e638c2e1e85ef90c4ae3c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_064c50efc4124fd4f64b42c73e16645a330d7dc6a7e638c2e1e85ef90c4ae3c1->enter($__internal_064c50efc4124fd4f64b42c73e16645a330d7dc6a7e638c2e1e85ef90c4ae3c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_9ba2d6d8cb012a52f49f0d3a804709905ce33aa5e77de1ec623e46da616a010b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ba2d6d8cb012a52f49f0d3a804709905ce33aa5e77de1ec623e46da616a010b->enter($__internal_9ba2d6d8cb012a52f49f0d3a804709905ce33aa5e77de1ec623e46da616a010b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_9ba2d6d8cb012a52f49f0d3a804709905ce33aa5e77de1ec623e46da616a010b->leave($__internal_9ba2d6d8cb012a52f49f0d3a804709905ce33aa5e77de1ec623e46da616a010b_prof);

        
        $__internal_064c50efc4124fd4f64b42c73e16645a330d7dc6a7e638c2e1e85ef90c4ae3c1->leave($__internal_064c50efc4124fd4f64b42c73e16645a330d7dc6a7e638c2e1e85ef90c4ae3c1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\web-profiler-bundle\\Resources\\views\\Collector\\ajax.html.twig");
    }
}
