<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_3e7c9bc668c4035b22e90fca2caddfc228e5ae4503f5c2d71c33eb8a2dfc81e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_43ab066f0ece69d874d4fcf49249e8bc1f476f5cf8f3227e8d69a4972664053a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43ab066f0ece69d874d4fcf49249e8bc1f476f5cf8f3227e8d69a4972664053a->enter($__internal_43ab066f0ece69d874d4fcf49249e8bc1f476f5cf8f3227e8d69a4972664053a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_51b52d69d4367338817937842f84f1da22cdd3dd5388be99b70173200c6b4f87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51b52d69d4367338817937842f84f1da22cdd3dd5388be99b70173200c6b4f87->enter($__internal_51b52d69d4367338817937842f84f1da22cdd3dd5388be99b70173200c6b4f87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_43ab066f0ece69d874d4fcf49249e8bc1f476f5cf8f3227e8d69a4972664053a->leave($__internal_43ab066f0ece69d874d4fcf49249e8bc1f476f5cf8f3227e8d69a4972664053a_prof);

        
        $__internal_51b52d69d4367338817937842f84f1da22cdd3dd5388be99b70173200c6b4f87->leave($__internal_51b52d69d4367338817937842f84f1da22cdd3dd5388be99b70173200c6b4f87_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_ad42cd9616b0d0abde43588f0475a13c83218eaba9007821b7ddd88d938df86b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad42cd9616b0d0abde43588f0475a13c83218eaba9007821b7ddd88d938df86b->enter($__internal_ad42cd9616b0d0abde43588f0475a13c83218eaba9007821b7ddd88d938df86b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2adf1304b336e70a25407573244df8f0fc227fdfb4cab62e4a37d0abd1423946 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2adf1304b336e70a25407573244df8f0fc227fdfb4cab62e4a37d0abd1423946->enter($__internal_2adf1304b336e70a25407573244df8f0fc227fdfb4cab62e4a37d0abd1423946_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_2adf1304b336e70a25407573244df8f0fc227fdfb4cab62e4a37d0abd1423946->leave($__internal_2adf1304b336e70a25407573244df8f0fc227fdfb4cab62e4a37d0abd1423946_prof);

        
        $__internal_ad42cd9616b0d0abde43588f0475a13c83218eaba9007821b7ddd88d938df86b->leave($__internal_ad42cd9616b0d0abde43588f0475a13c83218eaba9007821b7ddd88d938df86b_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_d73c7c9d16aa96398e299f043a6c0442c4f93a56dc9237ed1c0bb8fcc623501e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d73c7c9d16aa96398e299f043a6c0442c4f93a56dc9237ed1c0bb8fcc623501e->enter($__internal_d73c7c9d16aa96398e299f043a6c0442c4f93a56dc9237ed1c0bb8fcc623501e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_24e1bf6cb6d26756dc46b42e96ecb1e502d84b8bbcff4c5f439ae597070244dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24e1bf6cb6d26756dc46b42e96ecb1e502d84b8bbcff4c5f439ae597070244dd->enter($__internal_24e1bf6cb6d26756dc46b42e96ecb1e502d84b8bbcff4c5f439ae597070244dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_24e1bf6cb6d26756dc46b42e96ecb1e502d84b8bbcff4c5f439ae597070244dd->leave($__internal_24e1bf6cb6d26756dc46b42e96ecb1e502d84b8bbcff4c5f439ae597070244dd_prof);

        
        $__internal_d73c7c9d16aa96398e299f043a6c0442c4f93a56dc9237ed1c0bb8fcc623501e->leave($__internal_d73c7c9d16aa96398e299f043a6c0442c4f93a56dc9237ed1c0bb8fcc623501e_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_4ae68c54c0780c6b4bb86b3d8bf165f18143eba3b44ff2bb575b3304c377ae06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ae68c54c0780c6b4bb86b3d8bf165f18143eba3b44ff2bb575b3304c377ae06->enter($__internal_4ae68c54c0780c6b4bb86b3d8bf165f18143eba3b44ff2bb575b3304c377ae06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_50640174a112783bcbff76ae5c444b6e319c914733df7c57c086826d897fd43f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50640174a112783bcbff76ae5c444b6e319c914733df7c57c086826d897fd43f->enter($__internal_50640174a112783bcbff76ae5c444b6e319c914733df7c57c086826d897fd43f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_50640174a112783bcbff76ae5c444b6e319c914733df7c57c086826d897fd43f->leave($__internal_50640174a112783bcbff76ae5c444b6e319c914733df7c57c086826d897fd43f_prof);

        
        $__internal_4ae68c54c0780c6b4bb86b3d8bf165f18143eba3b44ff2bb575b3304c377ae06->leave($__internal_4ae68c54c0780c6b4bb86b3d8bf165f18143eba3b44ff2bb575b3304c377ae06_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\twig-bundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
