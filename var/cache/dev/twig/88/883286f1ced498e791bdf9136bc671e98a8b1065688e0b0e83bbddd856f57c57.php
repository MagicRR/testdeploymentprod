<?php

/* @Twig/images/icon-support.svg */
class __TwigTemplate_3b983ebe19a8756f8ced9c8123512d8c406d51efb067a9410df8e410ca7e8d2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f7b3e8c4c19d02b8f20fd5af878325d4de2dfd443c13ff2e0fcb5be611e6f68 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f7b3e8c4c19d02b8f20fd5af878325d4de2dfd443c13ff2e0fcb5be611e6f68->enter($__internal_6f7b3e8c4c19d02b8f20fd5af878325d4de2dfd443c13ff2e0fcb5be611e6f68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-support.svg"));

        $__internal_52f190e2d7c7170eb3ecaf0421d91f6a126e254432d3fc54c67d78f2d62127f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_52f190e2d7c7170eb3ecaf0421d91f6a126e254432d3fc54c67d78f2d62127f2->enter($__internal_52f190e2d7c7170eb3ecaf0421d91f6a126e254432d3fc54c67d78f2d62127f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-support.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M896 0q182 0 348 71t286 191 191 286 71 348-71 348-191 286-286 191-348 71-348-71-286-191-191-286T0 896t71-348 191-286T548 71 896 0zm0 128q-190 0-361 90l194 194q82-28 167-28t167 28l194-194q-171-90-361-90zM218 1257l194-194q-28-82-28-167t28-167L218 535q-90 171-90 361t90 361zm678 407q190 0 361-90l-194-194q-82 28-167 28t-167-28l-194 194q171 90 361 90zm0-384q159 0 271.5-112.5T1280 896t-112.5-271.5T896 512 624.5 624.5 512 896t112.5 271.5T896 1280zm484-217l194 194q90-171 90-361t-90-361l-194 194q28 82 28 167t-28 167z\"/></svg>
";
        
        $__internal_6f7b3e8c4c19d02b8f20fd5af878325d4de2dfd443c13ff2e0fcb5be611e6f68->leave($__internal_6f7b3e8c4c19d02b8f20fd5af878325d4de2dfd443c13ff2e0fcb5be611e6f68_prof);

        
        $__internal_52f190e2d7c7170eb3ecaf0421d91f6a126e254432d3fc54c67d78f2d62127f2->leave($__internal_52f190e2d7c7170eb3ecaf0421d91f6a126e254432d3fc54c67d78f2d62127f2_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-support.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M896 0q182 0 348 71t286 191 191 286 71 348-71 348-191 286-286 191-348 71-348-71-286-191-191-286T0 896t71-348 191-286T548 71 896 0zm0 128q-190 0-361 90l194 194q82-28 167-28t167 28l194-194q-171-90-361-90zM218 1257l194-194q-28-82-28-167t28-167L218 535q-90 171-90 361t90 361zm678 407q190 0 361-90l-194-194q-82 28-167 28t-167-28l-194 194q171 90 361 90zm0-384q159 0 271.5-112.5T1280 896t-112.5-271.5T896 512 624.5 624.5 512 896t112.5 271.5T896 1280zm484-217l194 194q90-171 90-361t-90-361l-194 194q28 82 28 167t-28 167z\"/></svg>
", "@Twig/images/icon-support.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\twig-bundle\\Resources\\views\\images\\icon-support.svg");
    }
}
