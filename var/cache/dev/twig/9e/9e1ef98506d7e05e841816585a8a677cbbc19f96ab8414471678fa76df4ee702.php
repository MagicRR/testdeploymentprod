<?php

/* @WebProfiler/Icon/ajax.svg */
class __TwigTemplate_c8b9cb4de26c897f5a20db57d9eba772d101abaedfae5bf33898ed245de8d8d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8aa064c3b5511f92df52f286fffb13a2bec48e4ea32cc0b21b42b2f84ff3a216 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8aa064c3b5511f92df52f286fffb13a2bec48e4ea32cc0b21b42b2f84ff3a216->enter($__internal_8aa064c3b5511f92df52f286fffb13a2bec48e4ea32cc0b21b42b2f84ff3a216_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/ajax.svg"));

        $__internal_a6faab74ee9173285cd6d3d8d215ae0f96a17276f4f407ab8304e078d08f8d1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6faab74ee9173285cd6d3d8d215ae0f96a17276f4f407ab8304e078d08f8d1a->enter($__internal_a6faab74ee9173285cd6d3d8d215ae0f96a17276f4f407ab8304e078d08f8d1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/ajax.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M9.8,18l-3.8,4.4c-0.3,0.3-0.8,0.4-1.1,0L1,18c-0.4-0.5-0.1-1,0.5-1H3V6.4C3,3.8,5.5,2,8.2,2h3.9
    c1.1,0,2,0.9,2,2s-0.9,2-2,2H8.2C7.7,6,7,6,7,6.4V17h2.2C9.8,17,10.2,17.5,9.8,18z M23,6l-3.8-4.5c-0.3-0.3-0.8-0.3-1.1,0L14.2,6
    c-0.4,0.5-0.1,1,0.5,1H17v10.6c0,0.4-0.7,0.4-1.2,0.4h-3.9c-1.1,0-2,0.9-2,2s0.9,2,2,2h3.9c2.6,0,5.2-1.8,5.2-4.4V7h1.5
    C23.1,7,23.4,6.5,23,6z\"/>
</svg>
";
        
        $__internal_8aa064c3b5511f92df52f286fffb13a2bec48e4ea32cc0b21b42b2f84ff3a216->leave($__internal_8aa064c3b5511f92df52f286fffb13a2bec48e4ea32cc0b21b42b2f84ff3a216_prof);

        
        $__internal_a6faab74ee9173285cd6d3d8d215ae0f96a17276f4f407ab8304e078d08f8d1a->leave($__internal_a6faab74ee9173285cd6d3d8d215ae0f96a17276f4f407ab8304e078d08f8d1a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/ajax.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M9.8,18l-3.8,4.4c-0.3,0.3-0.8,0.4-1.1,0L1,18c-0.4-0.5-0.1-1,0.5-1H3V6.4C3,3.8,5.5,2,8.2,2h3.9
    c1.1,0,2,0.9,2,2s-0.9,2-2,2H8.2C7.7,6,7,6,7,6.4V17h2.2C9.8,17,10.2,17.5,9.8,18z M23,6l-3.8-4.5c-0.3-0.3-0.8-0.3-1.1,0L14.2,6
    c-0.4,0.5-0.1,1,0.5,1H17v10.6c0,0.4-0.7,0.4-1.2,0.4h-3.9c-1.1,0-2,0.9-2,2s0.9,2,2,2h3.9c2.6,0,5.2-1.8,5.2-4.4V7h1.5
    C23.1,7,23.4,6.5,23,6z\"/>
</svg>
", "@WebProfiler/Icon/ajax.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\web-profiler-bundle\\Resources\\views\\Icon\\ajax.svg");
    }
}
