<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_e403fe32dee11ee7ac082dd99b4921ae9aea502c566c3ffb0791fea66acbac49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0014f7eb73e15133f0f917d10048291e00aa67a76c4b232820d866991aaaa58a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0014f7eb73e15133f0f917d10048291e00aa67a76c4b232820d866991aaaa58a->enter($__internal_0014f7eb73e15133f0f917d10048291e00aa67a76c4b232820d866991aaaa58a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_a41809f95dd14811bc225670154ff433e105f52efa74c8eab1b4f49284f2ba6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a41809f95dd14811bc225670154ff433e105f52efa74c8eab1b4f49284f2ba6e->enter($__internal_a41809f95dd14811bc225670154ff433e105f52efa74c8eab1b4f49284f2ba6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_0014f7eb73e15133f0f917d10048291e00aa67a76c4b232820d866991aaaa58a->leave($__internal_0014f7eb73e15133f0f917d10048291e00aa67a76c4b232820d866991aaaa58a_prof);

        
        $__internal_a41809f95dd14811bc225670154ff433e105f52efa74c8eab1b4f49284f2ba6e->leave($__internal_a41809f95dd14811bc225670154ff433e105f52efa74c8eab1b4f49284f2ba6e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\twig-bundle\\Resources\\views\\images\\chevron-right.svg");
    }
}
