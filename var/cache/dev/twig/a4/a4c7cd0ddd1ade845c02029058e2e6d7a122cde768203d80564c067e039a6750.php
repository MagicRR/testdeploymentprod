<?php

/* @Twig/images/icon-plus-square.svg */
class __TwigTemplate_cc903c1b338a0977b86087e9b74a5fa600841eb0f5cc92a69026389a8f8017db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26332a57ae381e5849c3d85e58f6ed51e3f2f59c2518880c6896ce1c785f11b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26332a57ae381e5849c3d85e58f6ed51e3f2f59c2518880c6896ce1c785f11b4->enter($__internal_26332a57ae381e5849c3d85e58f6ed51e3f2f59c2518880c6896ce1c785f11b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        $__internal_588b1c5321c94bae2ff064fa57b7f411768ad29d70f935c30a4feafb4c81793d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_588b1c5321c94bae2ff064fa57b7f411768ad29d70f935c30a4feafb4c81793d->enter($__internal_588b1c5321c94bae2ff064fa57b7f411768ad29d70f935c30a4feafb4c81793d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
";
        
        $__internal_26332a57ae381e5849c3d85e58f6ed51e3f2f59c2518880c6896ce1c785f11b4->leave($__internal_26332a57ae381e5849c3d85e58f6ed51e3f2f59c2518880c6896ce1c785f11b4_prof);

        
        $__internal_588b1c5321c94bae2ff064fa57b7f411768ad29d70f935c30a4feafb4c81793d->leave($__internal_588b1c5321c94bae2ff064fa57b7f411768ad29d70f935c30a4feafb4c81793d_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
", "@Twig/images/icon-plus-square.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\twig-bundle\\Resources\\views\\images\\icon-plus-square.svg");
    }
}
