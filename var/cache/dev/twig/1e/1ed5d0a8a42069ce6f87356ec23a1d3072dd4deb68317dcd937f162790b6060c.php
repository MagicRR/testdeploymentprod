<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_8a615b036d9d03b3eff7647880e8671ca4d67acd2efebbdc9a5423d02e71b7dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d38861bcf9b21eabb14735d539f859ff0ba29874c1d071bba6bcc118e430013 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d38861bcf9b21eabb14735d539f859ff0ba29874c1d071bba6bcc118e430013->enter($__internal_1d38861bcf9b21eabb14735d539f859ff0ba29874c1d071bba6bcc118e430013_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_e72000abfdd8c8460b7328c6f8a1f6275a0eab041c18d05f2966450184df60ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e72000abfdd8c8460b7328c6f8a1f6275a0eab041c18d05f2966450184df60ce->enter($__internal_e72000abfdd8c8460b7328c6f8a1f6275a0eab041c18d05f2966450184df60ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_1d38861bcf9b21eabb14735d539f859ff0ba29874c1d071bba6bcc118e430013->leave($__internal_1d38861bcf9b21eabb14735d539f859ff0ba29874c1d071bba6bcc118e430013_prof);

        
        $__internal_e72000abfdd8c8460b7328c6f8a1f6275a0eab041c18d05f2966450184df60ce->leave($__internal_e72000abfdd8c8460b7328c6f8a1f6275a0eab041c18d05f2966450184df60ce_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_460b3ba71ec5881f741c58fb6326ebad6eee125ca01be17b3253e1ef8914729a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_460b3ba71ec5881f741c58fb6326ebad6eee125ca01be17b3253e1ef8914729a->enter($__internal_460b3ba71ec5881f741c58fb6326ebad6eee125ca01be17b3253e1ef8914729a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_aa9e927deea364bd46f9f681af8d418b1832a24dbcdfd507e2e3463af26a3a82 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa9e927deea364bd46f9f681af8d418b1832a24dbcdfd507e2e3463af26a3a82->enter($__internal_aa9e927deea364bd46f9f681af8d418b1832a24dbcdfd507e2e3463af26a3a82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_aa9e927deea364bd46f9f681af8d418b1832a24dbcdfd507e2e3463af26a3a82->leave($__internal_aa9e927deea364bd46f9f681af8d418b1832a24dbcdfd507e2e3463af26a3a82_prof);

        
        $__internal_460b3ba71ec5881f741c58fb6326ebad6eee125ca01be17b3253e1ef8914729a->leave($__internal_460b3ba71ec5881f741c58fb6326ebad6eee125ca01be17b3253e1ef8914729a_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_94767c53c3e03a878733c324e90846c17e6d1f2a6f4b1582c2f66124ebf097eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94767c53c3e03a878733c324e90846c17e6d1f2a6f4b1582c2f66124ebf097eb->enter($__internal_94767c53c3e03a878733c324e90846c17e6d1f2a6f4b1582c2f66124ebf097eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_32c182511ef04a363be0ce1ce3123b1bfe797dd8f9c9becaf798ad0bfa0b833a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32c182511ef04a363be0ce1ce3123b1bfe797dd8f9c9becaf798ad0bfa0b833a->enter($__internal_32c182511ef04a363be0ce1ce3123b1bfe797dd8f9c9becaf798ad0bfa0b833a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_32c182511ef04a363be0ce1ce3123b1bfe797dd8f9c9becaf798ad0bfa0b833a->leave($__internal_32c182511ef04a363be0ce1ce3123b1bfe797dd8f9c9becaf798ad0bfa0b833a_prof);

        
        $__internal_94767c53c3e03a878733c324e90846c17e6d1f2a6f4b1582c2f66124ebf097eb->leave($__internal_94767c53c3e03a878733c324e90846c17e6d1f2a6f4b1582c2f66124ebf097eb_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_c73b32c8231f63afcb5f1e54f8299dcc4f6f6f3b9c88ca10a5cd27b61729156c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c73b32c8231f63afcb5f1e54f8299dcc4f6f6f3b9c88ca10a5cd27b61729156c->enter($__internal_c73b32c8231f63afcb5f1e54f8299dcc4f6f6f3b9c88ca10a5cd27b61729156c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d5f7f38722acefbc9b8c91e962ec9edab731cca8213df1912cd4bf7b732f5e60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5f7f38722acefbc9b8c91e962ec9edab731cca8213df1912cd4bf7b732f5e60->enter($__internal_d5f7f38722acefbc9b8c91e962ec9edab731cca8213df1912cd4bf7b732f5e60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_d5f7f38722acefbc9b8c91e962ec9edab731cca8213df1912cd4bf7b732f5e60->leave($__internal_d5f7f38722acefbc9b8c91e962ec9edab731cca8213df1912cd4bf7b732f5e60_prof);

        
        $__internal_c73b32c8231f63afcb5f1e54f8299dcc4f6f6f3b9c88ca10a5cd27b61729156c->leave($__internal_c73b32c8231f63afcb5f1e54f8299dcc4f6f6f3b9c88ca10a5cd27b61729156c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\twig-bundle\\Resources\\views\\layout.html.twig");
    }
}
