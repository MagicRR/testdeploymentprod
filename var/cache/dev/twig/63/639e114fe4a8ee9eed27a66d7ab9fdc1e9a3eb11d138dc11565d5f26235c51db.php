<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_94f47d6e05e567342a7c43e130ea2881d50439fe03b9b6b04a2791455af36e69 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81cbc8297a5d0dd9a9bc69965dc497493538ae3ab53a04238bb5ce6f1ee60be5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81cbc8297a5d0dd9a9bc69965dc497493538ae3ab53a04238bb5ce6f1ee60be5->enter($__internal_81cbc8297a5d0dd9a9bc69965dc497493538ae3ab53a04238bb5ce6f1ee60be5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_b6b9ee34df649fa92cb2add1bd0300928d442c945c55744498fd0b9b57227c1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6b9ee34df649fa92cb2add1bd0300928d442c945c55744498fd0b9b57227c1f->enter($__internal_b6b9ee34df649fa92cb2add1bd0300928d442c945c55744498fd0b9b57227c1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_81cbc8297a5d0dd9a9bc69965dc497493538ae3ab53a04238bb5ce6f1ee60be5->leave($__internal_81cbc8297a5d0dd9a9bc69965dc497493538ae3ab53a04238bb5ce6f1ee60be5_prof);

        
        $__internal_b6b9ee34df649fa92cb2add1bd0300928d442c945c55744498fd0b9b57227c1f->leave($__internal_b6b9ee34df649fa92cb2add1bd0300928d442c945c55744498fd0b9b57227c1f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_0956f4a6dc1c637bb75c770ea17e1d762a5bb9f70d2ae2269060efbe088f5696 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0956f4a6dc1c637bb75c770ea17e1d762a5bb9f70d2ae2269060efbe088f5696->enter($__internal_0956f4a6dc1c637bb75c770ea17e1d762a5bb9f70d2ae2269060efbe088f5696_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_86abdfe71deb869798a8dd908aa8469c1432fbad23635a3df732c4ed2f5e649b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86abdfe71deb869798a8dd908aa8469c1432fbad23635a3df732c4ed2f5e649b->enter($__internal_86abdfe71deb869798a8dd908aa8469c1432fbad23635a3df732c4ed2f5e649b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_86abdfe71deb869798a8dd908aa8469c1432fbad23635a3df732c4ed2f5e649b->leave($__internal_86abdfe71deb869798a8dd908aa8469c1432fbad23635a3df732c4ed2f5e649b_prof);

        
        $__internal_0956f4a6dc1c637bb75c770ea17e1d762a5bb9f70d2ae2269060efbe088f5696->leave($__internal_0956f4a6dc1c637bb75c770ea17e1d762a5bb9f70d2ae2269060efbe088f5696_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6e3a72916ce1981bbe1b63023e74b1729c553613b3907c6694f08a3a90d1f10b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e3a72916ce1981bbe1b63023e74b1729c553613b3907c6694f08a3a90d1f10b->enter($__internal_6e3a72916ce1981bbe1b63023e74b1729c553613b3907c6694f08a3a90d1f10b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_19e473d47907ed71ca0b7b65b7f4118fa35909604b92df382b83cb689b35d076 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19e473d47907ed71ca0b7b65b7f4118fa35909604b92df382b83cb689b35d076->enter($__internal_19e473d47907ed71ca0b7b65b7f4118fa35909604b92df382b83cb689b35d076_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_19e473d47907ed71ca0b7b65b7f4118fa35909604b92df382b83cb689b35d076->leave($__internal_19e473d47907ed71ca0b7b65b7f4118fa35909604b92df382b83cb689b35d076_prof);

        
        $__internal_6e3a72916ce1981bbe1b63023e74b1729c553613b3907c6694f08a3a90d1f10b->leave($__internal_6e3a72916ce1981bbe1b63023e74b1729c553613b3907c6694f08a3a90d1f10b_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_87d6f77f371ea24be709834a7233955d6f18dd0119e45072132c06045c1d26cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_87d6f77f371ea24be709834a7233955d6f18dd0119e45072132c06045c1d26cc->enter($__internal_87d6f77f371ea24be709834a7233955d6f18dd0119e45072132c06045c1d26cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_d9613fabd011b6e34be533a0c1f347257c34749ef1ed05dead8e213dbecac9a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9613fabd011b6e34be533a0c1f347257c34749ef1ed05dead8e213dbecac9a5->enter($__internal_d9613fabd011b6e34be533a0c1f347257c34749ef1ed05dead8e213dbecac9a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_d9613fabd011b6e34be533a0c1f347257c34749ef1ed05dead8e213dbecac9a5->leave($__internal_d9613fabd011b6e34be533a0c1f347257c34749ef1ed05dead8e213dbecac9a5_prof);

        
        $__internal_87d6f77f371ea24be709834a7233955d6f18dd0119e45072132c06045c1d26cc->leave($__internal_87d6f77f371ea24be709834a7233955d6f18dd0119e45072132c06045c1d26cc_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\web-profiler-bundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
