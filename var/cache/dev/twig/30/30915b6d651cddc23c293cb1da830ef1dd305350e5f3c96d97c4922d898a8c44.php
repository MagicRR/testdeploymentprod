<?php

/* accueil/accueil.html.twig */
class __TwigTemplate_854fbf696fd931e534b4d3b41a1627bfc193a2874a6caf76a7bb91750390a47a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("base.html.twig", "accueil/accueil.html.twig", 4);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e5797eac5feaf02ca791359517629424bb8967d1d7a82b691100e11db984905 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e5797eac5feaf02ca791359517629424bb8967d1d7a82b691100e11db984905->enter($__internal_8e5797eac5feaf02ca791359517629424bb8967d1d7a82b691100e11db984905_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "accueil/accueil.html.twig"));

        $__internal_aca6e6f0957fb391f48d6ab48aa0a46a284a0b09d2055bd37c9702b30cf21fe9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aca6e6f0957fb391f48d6ab48aa0a46a284a0b09d2055bd37c9702b30cf21fe9->enter($__internal_aca6e6f0957fb391f48d6ab48aa0a46a284a0b09d2055bd37c9702b30cf21fe9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "accueil/accueil.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8e5797eac5feaf02ca791359517629424bb8967d1d7a82b691100e11db984905->leave($__internal_8e5797eac5feaf02ca791359517629424bb8967d1d7a82b691100e11db984905_prof);

        
        $__internal_aca6e6f0957fb391f48d6ab48aa0a46a284a0b09d2055bd37c9702b30cf21fe9->leave($__internal_aca6e6f0957fb391f48d6ab48aa0a46a284a0b09d2055bd37c9702b30cf21fe9_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_e9f9ea4c793ad32471d935b729baba4308b9c9a0288d4ee436cf5115be09b17d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9f9ea4c793ad32471d935b729baba4308b9c9a0288d4ee436cf5115be09b17d->enter($__internal_e9f9ea4c793ad32471d935b729baba4308b9c9a0288d4ee436cf5115be09b17d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1a78f9bca386a955d34dddbb362702f436866fe5b81fcedbd0d90e379c6fa8eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a78f9bca386a955d34dddbb362702f436866fe5b81fcedbd0d90e379c6fa8eb->enter($__internal_1a78f9bca386a955d34dddbb362702f436866fe5b81fcedbd0d90e379c6fa8eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 7
        echo "    Accueil Recycling Carbon
";
        
        $__internal_1a78f9bca386a955d34dddbb362702f436866fe5b81fcedbd0d90e379c6fa8eb->leave($__internal_1a78f9bca386a955d34dddbb362702f436866fe5b81fcedbd0d90e379c6fa8eb_prof);

        
        $__internal_e9f9ea4c793ad32471d935b729baba4308b9c9a0288d4ee436cf5115be09b17d->leave($__internal_e9f9ea4c793ad32471d935b729baba4308b9c9a0288d4ee436cf5115be09b17d_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_0f882cc3eb89c7ff3541583ea77366ac37607426f39af3382948508c43a9e197 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f882cc3eb89c7ff3541583ea77366ac37607426f39af3382948508c43a9e197->enter($__internal_0f882cc3eb89c7ff3541583ea77366ac37607426f39af3382948508c43a9e197_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bbeb296b3a94b6175d11b1f7c570ccad08c50251f41866b1e72fbb866bfb4ceb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbeb296b3a94b6175d11b1f7c570ccad08c50251f41866b1e72fbb866bfb4ceb->enter($__internal_bbeb296b3a94b6175d11b1f7c570ccad08c50251f41866b1e72fbb866bfb4ceb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "
    ";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["userInformations"]) || array_key_exists("userInformations", $context) ? $context["userInformations"] : (function () { throw new Twig_Error_Runtime('Variable "userInformations" does not exist.', 13, $this->getSourceContext()); })()), "html", null, true);
        echo "

";
        
        $__internal_bbeb296b3a94b6175d11b1f7c570ccad08c50251f41866b1e72fbb866bfb4ceb->leave($__internal_bbeb296b3a94b6175d11b1f7c570ccad08c50251f41866b1e72fbb866bfb4ceb_prof);

        
        $__internal_0f882cc3eb89c7ff3541583ea77366ac37607426f39af3382948508c43a9e197->leave($__internal_0f882cc3eb89c7ff3541583ea77366ac37607426f39af3382948508c43a9e197_prof);

    }

    public function getTemplateName()
    {
        return "accueil/accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  70 => 12,  61 => 11,  50 => 7,  41 => 6,  11 => 4,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# templates/accueil/accueil.html.twig #}

{# Extends #}
{% extends \"base.html.twig\" %}

{% block title %}
    Accueil Recycling Carbon
{% endblock %}


{% block body %}

    {{ userInformations }}

{% endblock %}
", "accueil/accueil.html.twig", "D:\\wamp64\\www\\TestDeploymentProd\\templates\\accueil\\accueil.html.twig");
    }
}
