<?php

/* @Twig/images/icon-minus-square.svg */
class __TwigTemplate_a86af60870b935baf23b06bf035d19db9301dad3e4e38e5b44efeaf58a1cb87b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83d2dbcfc8a03c16833222548c0cf9e8f43c9571465bd62068656e6a99775d9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83d2dbcfc8a03c16833222548c0cf9e8f43c9571465bd62068656e6a99775d9d->enter($__internal_83d2dbcfc8a03c16833222548c0cf9e8f43c9571465bd62068656e6a99775d9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $__internal_8d20091072c978f6691976d64d8cf361cabbee4004b749ea1105b192ecb97bc6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d20091072c978f6691976d64d8cf361cabbee4004b749ea1105b192ecb97bc6->enter($__internal_8d20091072c978f6691976d64d8cf361cabbee4004b749ea1105b192ecb97bc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $__internal_83d2dbcfc8a03c16833222548c0cf9e8f43c9571465bd62068656e6a99775d9d->leave($__internal_83d2dbcfc8a03c16833222548c0cf9e8f43c9571465bd62068656e6a99775d9d_prof);

        
        $__internal_8d20091072c978f6691976d64d8cf361cabbee4004b749ea1105b192ecb97bc6->leave($__internal_8d20091072c978f6691976d64d8cf361cabbee4004b749ea1105b192ecb97bc6_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\twig-bundle\\Resources\\views\\images\\icon-minus-square.svg");
    }
}
