<?php

/* @Twig/images/icon-minus-square-o.svg */
class __TwigTemplate_08f9ae9e01bf08399defb53b8648342f2712c7ee094269e61d71cef10b9ff631 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4077a106f92bf7fdce62cff80e4591338c00fac91e9c02401d5e99a17fc2fb6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4077a106f92bf7fdce62cff80e4591338c00fac91e9c02401d5e99a17fc2fb6a->enter($__internal_4077a106f92bf7fdce62cff80e4591338c00fac91e9c02401d5e99a17fc2fb6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        $__internal_c38d2388616d824dfd5937dccc3346a437ec1388f288a90eb8061f11be360f5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c38d2388616d824dfd5937dccc3346a437ec1388f288a90eb8061f11be360f5c->enter($__internal_c38d2388616d824dfd5937dccc3346a437ec1388f288a90eb8061f11be360f5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
";
        
        $__internal_4077a106f92bf7fdce62cff80e4591338c00fac91e9c02401d5e99a17fc2fb6a->leave($__internal_4077a106f92bf7fdce62cff80e4591338c00fac91e9c02401d5e99a17fc2fb6a_prof);

        
        $__internal_c38d2388616d824dfd5937dccc3346a437ec1388f288a90eb8061f11be360f5c->leave($__internal_c38d2388616d824dfd5937dccc3346a437ec1388f288a90eb8061f11be360f5c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square-o.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
", "@Twig/images/icon-minus-square-o.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\twig-bundle\\Resources\\views\\images\\icon-minus-square-o.svg");
    }
}
