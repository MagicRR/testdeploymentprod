<?php

/* @WebProfiler/Icon/twig.svg */
class __TwigTemplate_5d0bed06b10d4904f9ea61b64ffada37cfb7ae3bae092994f0572a308e012de8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_00e323410852c32924e2ad9152088911bd975044199e2d1794cceeb8d0be36f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00e323410852c32924e2ad9152088911bd975044199e2d1794cceeb8d0be36f0->enter($__internal_00e323410852c32924e2ad9152088911bd975044199e2d1794cceeb8d0be36f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        $__internal_0ecc8aa9833d66eaafa777bce9ac9a7b754306c4f4f62ba02d5f366b6ed19fc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ecc8aa9833d66eaafa777bce9ac9a7b754306c4f4f62ba02d5f366b6ed19fc5->enter($__internal_0ecc8aa9833d66eaafa777bce9ac9a7b754306c4f4f62ba02d5f366b6ed19fc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
";
        
        $__internal_00e323410852c32924e2ad9152088911bd975044199e2d1794cceeb8d0be36f0->leave($__internal_00e323410852c32924e2ad9152088911bd975044199e2d1794cceeb8d0be36f0_prof);

        
        $__internal_0ecc8aa9833d66eaafa777bce9ac9a7b754306c4f4f62ba02d5f366b6ed19fc5->leave($__internal_0ecc8aa9833d66eaafa777bce9ac9a7b754306c4f4f62ba02d5f366b6ed19fc5_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/twig.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
", "@WebProfiler/Icon/twig.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\web-profiler-bundle\\Resources\\views\\Icon\\twig.svg");
    }
}
