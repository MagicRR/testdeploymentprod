<?php

/* @WebProfiler/Icon/cache.svg */
class __TwigTemplate_9183d7138e424e7cb9eaadcddfc727081d90bc41f0a3920c3e577a84147f6793 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_faad685342e112161cb57beb294ca3e8822a1650b5221305b2e9e18255babe40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_faad685342e112161cb57beb294ca3e8822a1650b5221305b2e9e18255babe40->enter($__internal_faad685342e112161cb57beb294ca3e8822a1650b5221305b2e9e18255babe40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/cache.svg"));

        $__internal_19460f3571ed1d3912825892e25860172e1851c52e5e5f72b5e94139e66d674b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19460f3571ed1d3912825892e25860172e1851c52e5e5f72b5e94139e66d674b->enter($__internal_19460f3571ed1d3912825892e25860172e1851c52e5e5f72b5e94139e66d674b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/cache.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAA\" d=\"M2.26 6.09l9.06-4.67a1.49 1.49 0 0 1 1.37 0l9.06 4.67a1.49 1.49 0 0 1 0 2.65l-9.06 4.67a1.49 1.49 0 0 1-1.37 0L2.26 8.74a1.49 1.49 0 0 1 0-2.65zM20.55 11L12 15.39 3.45 11a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73A1.36 1.36 0 0 0 20.55 11zm0 4.47L12 19.86l-8.55-4.41a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73a1.36 1.36 0 0 0-1.25-2.42z\"/>
</svg>
";
        
        $__internal_faad685342e112161cb57beb294ca3e8822a1650b5221305b2e9e18255babe40->leave($__internal_faad685342e112161cb57beb294ca3e8822a1650b5221305b2e9e18255babe40_prof);

        
        $__internal_19460f3571ed1d3912825892e25860172e1851c52e5e5f72b5e94139e66d674b->leave($__internal_19460f3571ed1d3912825892e25860172e1851c52e5e5f72b5e94139e66d674b_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/cache.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAA\" d=\"M2.26 6.09l9.06-4.67a1.49 1.49 0 0 1 1.37 0l9.06 4.67a1.49 1.49 0 0 1 0 2.65l-9.06 4.67a1.49 1.49 0 0 1-1.37 0L2.26 8.74a1.49 1.49 0 0 1 0-2.65zM20.55 11L12 15.39 3.45 11a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73A1.36 1.36 0 0 0 20.55 11zm0 4.47L12 19.86l-8.55-4.41a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73a1.36 1.36 0 0 0-1.25-2.42z\"/>
</svg>
", "@WebProfiler/Icon/cache.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\web-profiler-bundle\\Resources\\views\\Icon\\cache.svg");
    }
}
