<?php

/* @WebProfiler/Icon/close.svg */
class __TwigTemplate_a8efb6f19f44e874e5c2ab6d9c6ae6c4ada3a3fae98b733315bab4d0b3206ed7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0d48716709b54f0d58b692b2f74715928d08e6f15fa0c55822ef6b4278a4af2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0d48716709b54f0d58b692b2f74715928d08e6f15fa0c55822ef6b4278a4af2->enter($__internal_f0d48716709b54f0d58b692b2f74715928d08e6f15fa0c55822ef6b4278a4af2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/close.svg"));

        $__internal_2348aca6737207c18ed9b7dab52f0a07819b1013e60a49ccaa8567ea2a539605 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2348aca6737207c18ed9b7dab52f0a07819b1013e60a49ccaa8567ea2a539605->enter($__internal_2348aca6737207c18ed9b7dab52f0a07819b1013e60a49ccaa8567ea2a539605_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/close.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M21.1,18.3c0.8,0.8,0.8,2,0,2.8c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6L12,14.8l-6.3,6.3
    c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6c-0.8-0.8-0.8-2,0-2.8L9.2,12L2.9,5.7c-0.8-0.8-0.8-2,0-2.8c0.8-0.8,2-0.8,2.8,0L12,9.2
    l6.3-6.3c0.8-0.8,2-0.8,2.8,0c0.8,0.8,0.8,2,0,2.8L14.8,12L21.1,18.3z\"/>
</svg>
";
        
        $__internal_f0d48716709b54f0d58b692b2f74715928d08e6f15fa0c55822ef6b4278a4af2->leave($__internal_f0d48716709b54f0d58b692b2f74715928d08e6f15fa0c55822ef6b4278a4af2_prof);

        
        $__internal_2348aca6737207c18ed9b7dab52f0a07819b1013e60a49ccaa8567ea2a539605->leave($__internal_2348aca6737207c18ed9b7dab52f0a07819b1013e60a49ccaa8567ea2a539605_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/close.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M21.1,18.3c0.8,0.8,0.8,2,0,2.8c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6L12,14.8l-6.3,6.3
    c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6c-0.8-0.8-0.8-2,0-2.8L9.2,12L2.9,5.7c-0.8-0.8-0.8-2,0-2.8c0.8-0.8,2-0.8,2.8,0L12,9.2
    l6.3-6.3c0.8-0.8,2-0.8,2.8,0c0.8,0.8,0.8,2,0,2.8L14.8,12L21.1,18.3z\"/>
</svg>
", "@WebProfiler/Icon/close.svg", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\web-profiler-bundle\\Resources\\views\\Icon\\close.svg");
    }
}
