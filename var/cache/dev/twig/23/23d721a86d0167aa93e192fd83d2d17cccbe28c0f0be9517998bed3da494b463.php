<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_db078300efcac1a80aa3e5dcfc10efe20ae586415c0c8596f1382f6c00fdbc97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd8d40daf6ee74e3e3e45ed6ccba51ea4d119c640db161e50469c157442e2485 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd8d40daf6ee74e3e3e45ed6ccba51ea4d119c640db161e50469c157442e2485->enter($__internal_cd8d40daf6ee74e3e3e45ed6ccba51ea4d119c640db161e50469c157442e2485_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_3015c5808e50bf2ba876a8f6b331de4e4a079245e49e054fbcaf9419ad5795ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3015c5808e50bf2ba876a8f6b331de4e4a079245e49e054fbcaf9419ad5795ab->enter($__internal_3015c5808e50bf2ba876a8f6b331de4e4a079245e49e054fbcaf9419ad5795ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cd8d40daf6ee74e3e3e45ed6ccba51ea4d119c640db161e50469c157442e2485->leave($__internal_cd8d40daf6ee74e3e3e45ed6ccba51ea4d119c640db161e50469c157442e2485_prof);

        
        $__internal_3015c5808e50bf2ba876a8f6b331de4e4a079245e49e054fbcaf9419ad5795ab->leave($__internal_3015c5808e50bf2ba876a8f6b331de4e4a079245e49e054fbcaf9419ad5795ab_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_3d70b501b028d53a5a1a9691043f678b14611600118d2148519f4386eb0145b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d70b501b028d53a5a1a9691043f678b14611600118d2148519f4386eb0145b8->enter($__internal_3d70b501b028d53a5a1a9691043f678b14611600118d2148519f4386eb0145b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_a9d5e3b0daf2023b4a227107d5ef0b8d70a07dcf1b51b9cc8d205f7c0e969928 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9d5e3b0daf2023b4a227107d5ef0b8d70a07dcf1b51b9cc8d205f7c0e969928->enter($__internal_a9d5e3b0daf2023b4a227107d5ef0b8d70a07dcf1b51b9cc8d205f7c0e969928_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_a9d5e3b0daf2023b4a227107d5ef0b8d70a07dcf1b51b9cc8d205f7c0e969928->leave($__internal_a9d5e3b0daf2023b4a227107d5ef0b8d70a07dcf1b51b9cc8d205f7c0e969928_prof);

        
        $__internal_3d70b501b028d53a5a1a9691043f678b14611600118d2148519f4386eb0145b8->leave($__internal_3d70b501b028d53a5a1a9691043f678b14611600118d2148519f4386eb0145b8_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d0576873b8473997eaa6a523943fad76ee0b546f3ef9e5472a733687732b720c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0576873b8473997eaa6a523943fad76ee0b546f3ef9e5472a733687732b720c->enter($__internal_d0576873b8473997eaa6a523943fad76ee0b546f3ef9e5472a733687732b720c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_67ef809be226559d6b414bef469631ff92d1a7bb24f6f9ab55480c0803a07ad6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67ef809be226559d6b414bef469631ff92d1a7bb24f6f9ab55480c0803a07ad6->enter($__internal_67ef809be226559d6b414bef469631ff92d1a7bb24f6f9ab55480c0803a07ad6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_67ef809be226559d6b414bef469631ff92d1a7bb24f6f9ab55480c0803a07ad6->leave($__internal_67ef809be226559d6b414bef469631ff92d1a7bb24f6f9ab55480c0803a07ad6_prof);

        
        $__internal_d0576873b8473997eaa6a523943fad76ee0b546f3ef9e5472a733687732b720c->leave($__internal_d0576873b8473997eaa6a523943fad76ee0b546f3ef9e5472a733687732b720c_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_73757d3a689e068d1a4fec482b81a24f0f95b5ad3572ee4016b0f2e23b5900ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73757d3a689e068d1a4fec482b81a24f0f95b5ad3572ee4016b0f2e23b5900ff->enter($__internal_73757d3a689e068d1a4fec482b81a24f0f95b5ad3572ee4016b0f2e23b5900ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_0eb5c1cc6f07162cf7c86fb631ca50866c4eb747686ec9c5c3ddda54a780caff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0eb5c1cc6f07162cf7c86fb631ca50866c4eb747686ec9c5c3ddda54a780caff->enter($__internal_0eb5c1cc6f07162cf7c86fb631ca50866c4eb747686ec9c5c3ddda54a780caff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_0eb5c1cc6f07162cf7c86fb631ca50866c4eb747686ec9c5c3ddda54a780caff->leave($__internal_0eb5c1cc6f07162cf7c86fb631ca50866c4eb747686ec9c5c3ddda54a780caff_prof);

        
        $__internal_73757d3a689e068d1a4fec482b81a24f0f95b5ad3572ee4016b0f2e23b5900ff->leave($__internal_73757d3a689e068d1a4fec482b81a24f0f95b5ad3572ee4016b0f2e23b5900ff_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "D:\\wamp64\\www\\TestDeploymentProd\\vendor\\symfony\\web-profiler-bundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
